<?php
namespace Home\Model;
use Think\Model;

class UserModel extends Model{
	//自动验证的数据
	public $_validate = array(
		array('username','8,16','用户名的长度必须8到16位',1,'length',3),
		array('email','email','请输入合法的邮箱地址',1,'regex',3),
		array('password','6,16','密码的长度必须8到16位',1,'length',3),
		array('re_password','password','两次密码不一致',1,'confirm',3)
	);
}
?>