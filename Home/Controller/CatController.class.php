<?php
namespace Home\Controller;
use Think\Controller;
class CatController extends Controller {
    public function cat(){
    	$cat_id = I('cat_id')?I('cat_id'):0;
		//商品展示
		$goods = D('goods')->field('goods_id,goods_name,shop_price,goods_img,market_price')->where('cat_id ='.$cat_id)->select();
		$this->assign('goods',$goods); 
		//栏目分类
		$catlist = D('cat')->select();
		$this->assign('cat',$catlist);
		//热门商品
		$click_hot = D('goods')->field('goods_id,goods_name,is_best,is_hot,shop_price,goods_img,market_price,click_count')->where('is_on_sale = 1')->order('click_count desc')->limit(10)->select();
		$this->assign('click_hot',$click_hot);
		//历史浏览
		$this->assign('history',array_reverse(session('history')));
		//模板展示
       $this->display(); 
    }
}