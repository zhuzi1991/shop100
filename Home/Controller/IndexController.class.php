<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
    	$model = D('goods');
		//热销商品
		$goods_hot = $model->field('goods_id,goods_name,is_best,is_hot,shop_price,goods_img,market_price,click_count')->where('is_hot = 1')->order('goods_id desc')->limit(4)->select();
		$this->assign('goods_hot',$goods_hot);
		//最新商品
		$goods_new = $model->field('goods_id,goods_name,is_best,is_hot,shop_price,goods_img,market_price,click_count')->where('is_new = 1')->order('goods_id desc')->limit(4)->select();
		$this->assign('goods_new',$goods_new);
		//最佳商品
		$goods_best = $model->field('goods_id,goods_name,is_best,is_hot,shop_price,goods_img,market_price,click_count')->where('is_best = 1')->order('goods_id desc')->limit(4)->select();
		$this->assign('goods_best',$goods_best);
		//热门商品
		$click_hot = $model->field('goods_id,goods_name,is_best,is_hot,shop_price,goods_img,market_price,click_count')->where('is_on_sale = 1')->order('click_count desc')->limit(10)->select();
		$this->assign('click_hot',$click_hot);
		//欄目分類　
		$tree = D('Admin/Cat')->getTree();
		$tree1 = D('cat')->select();
		$this->assign('tree',$tree);
	
		//展示模板
		$this->display();
    }
}
