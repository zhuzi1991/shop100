<?php
namespace Home\Controller;
use Think\Controller;
class GoodsController extends Controller {
	public function goods() {
		$id = I('goods_id');
		if (!$id) {
			//没有接收到ID,转向首页
			$this -> redirect('home/index/index');
		} else {
			//商品信息
			$goods = D('goods') -> find($id);
			$this -> assign('goods', $goods);
			//面包屑导航
			$mbx = $this -> mbx($goods['cat_id']);
			$this -> assign('mbx', $mbx);
			//生成历史浏览
			$this->history($goods);
			$this -> display();
		}
	}
	//面包屑数组
	public function mbx($cat_id) {
		static $tree = array();
		$row = D('cat') -> find($cat_id);
		//给我栏目ID, 我查出这个栏目信息
		$tree[] = $row;
		//放入空数组
		if ($row['parent_id'] != 0) {//判断是否顶级栏目,如果不是,继续找
			$row = $this -> mbx($row['parent_id']);
			//递归,并把结果扔进数组
		}
		return array_reverse($tree);
	}
	
	//历史记录
	public function history($goods){
		$row = session('?history')?session('history'):array();
		$g = array();
		$g['goods_id'] = $goods['goods_id'];
		$g['goods_name'] = $goods['goods_name'];
		$g['shop_price'] = $goods['shop_price'];
		$row[$goods['goods_id']] = $g;
		
		if(count($row)>7){
			$key = key($row);
			unset($row[$key]);
		}
		session('history',$row);
	}

}
