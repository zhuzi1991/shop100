<?php
namespace Home\Controller;
use Think\Controller;
class UserController extends Controller {
	
	//用户注册
	public function register() {
		if (!IS_POST) {
			$this -> display();
		} else {
			$model = D('user');
			if (!$model -> create()) {
				echo $model -> getError();
			} else {
				$salt = mt_rand(10000, 99999);
				//盐
				$model -> salt = $salt;
				//盐入库
				$model -> password = md5($model->password.$salt);
				if (!$model -> add()) {
					echo $model -> getError();
				} else {
					$this -> success('注册成功,请登录', '/index.php/home/user/login', 2);
				}
			}
		}
	}
	
	//用户登录
	public function login() {
		if (!IS_POST) {
			$this -> display();
		} else {
			//检测验证码是否正确
			if (!$this -> checkyzm()) {
				exit('验证码不正确');
			}
			$usermodel = D('user')->where('username='."'".I('post.username')."'")->find(); //查询用户数据 
			//检测用户名是否存在
			if(!$usermodel){
				exit('用户名不存在,小兄弟你不要瞎掰~');
			}
			//检测密码是否正确
			$password = md5(I('post.password').$usermodel['salt']);
			if($usermodel['password'] !== $password){
				exit('密码不正确,请检查大小写');
			}
			//全部检测通过,创建cookie
			cookie('username',I('post.username')); 
			cookie('code',md5(I('post.username').C('DB_SALT')));
			$this->success('登录成功!','/index.php/home/index/index',2);
		}
	}
	
	//用户退出
	public function logout(){
		cookie('username',null);
		$this->redirect('/');
	}  
	
	//生成验证码,直接在H5页面调用方法地址
	public function yzm() {
		$Verify = new \Think\Verify();
		$Verify->useCurve = false; //不用曲线
		$Verify->useNoise = false; //不用杂点
		$Verify->length = 4 ;  //验证码位数
		$Verify->codeSet = '123456789'; //验证码字符集合
		$Verify -> entry();
	}

	//检测验证码,返回布尔值
	public function checkyzm() {
		$Verify = new \Think\Verify();
		return $Verify -> check(I('post.yzm'));
	}

}
