<?php
namespace Admin\Model;
use Think\Model;

class CatModel extends Model{
	public $cats = array();
	public function __construct(){  
		parent::__construct();   
		$this->cats = $this->select();  
	}
	
	//栏目无限分类
	public function getTree($parent_id=0,$lv=0){
		$tree = array();
		foreach ($this->cats as  $v) {
			$v['lv'] = $lv ;
			if($v['parent_id']==$parent_id){
				$tree[] = $v;  //顶层栏目
				$tree = array_merge($tree,$this->getTree($v['cat_id'],$lv+1));//合并
			}
		}
		return $tree;
	}
}

    
	  
	  


	
?>