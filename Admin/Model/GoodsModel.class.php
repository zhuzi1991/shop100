<?php
namespace Admin\Model;
use Think\Model;

class GoodsModel extends Model{
	//数据自动验证   
	protected $_validate = array(
		array('goods_name','6,16','必须是6~16个数字或汉字','','length',3),//这个字段长度必须在6~16位之间
		array('goods_email','email','请填写正确的邮箱地址','','')//这个字段必须是唯一的,不允许和数据库的重复
	);
//		array('goods_sn','unique','商品货号已存在,请重新填写','','unique'),//这个字段必须是唯一的,不允许和数据库的重复
	
	
	//数据的自动填充
	protected $_auto = array(
		array('add_time','time',1,'function'), //add_time字段,在新增的时候调用time函数  
		array('last_update','time',2,'function')//last_update字段,在修改的时候调用time函数
	);
	
	
	//新增数据的过滤 //在名单内的字段,才允许被新增 
	protected $insertFields = " goods_id,goods_sn,cat_id,goods_name,goods_number,goods_weight,shop_price,goods_desc,goods_brief,ori_img,goods_img,thumb_img,is_best,is_new,is_hot,is_sale,add_time ";
	
	//修改数据的过滤,在名单内的字段,才允许被修改. 可以避免敏感数据被修改, 比如商品SN
	protected $updateFields = " cat_id,goods_number,
goods_weight,shop_price,goods_desc,
goods_brief,ori_img,goods_img,thumb_img,is_best,
is_new,is_hot,is_sale ";

		//栏目无限分类
	public function getTree($parent_id=0,$lv=0){
		var_dump(1231223);
	}


}

	
?>