<?php
namespace admin\Controller;
use Think\Controller;
class goodsController extends Controller {
	//新增商品方法
	public function goodsAdd() {
		if (!IS_POST) {
			$this -> display();
		} else {
			$upload = new \Think\Upload();
			// 实例化上传类
			$upload -> maxSize = 10485760;
			// 设置附件上传大小,10M
			$upload -> exts = array('jpg', 'gif', 'png', 'jpeg');
			// 设置附件上传类型
			$upload -> rootPath = './public/uploads/goods_images/';
			// 设置附件上传根目录
			$upload -> savePath = '';
			// 设置附件上传（子）目录
			// 上传文件
			$info = $upload -> upload();
			if (!$info) {// 上传错误提示错误信息
				$this -> error($upload -> getError());
				exit();
			}
			//创建三个图的地址和数据对象
			$_POST['goods_img'] = 'Public/uploads/goods_images/'. $info['goods_img']['savepath'] . $info['goods_img']['savename'];
			$_POST['thumb_img'] = 'Public/uploads/goods_images/'. $info['goods_img']['savepath'] .'thumb_'. $info['goods_img']['savename'];
			$_POST['ori_img'] = 'Public/uploads/goods_images/'. $info['goods_img']['savepath'] .'ori_'. $info['goods_img']['savename'];

			//生成thumb缩略图
			$image = new \Think\Image();
			$image->open($_POST['goods_img']);
			$image->thumb(208,208)->save($_POST['thumb_img']); 
			
			//生成ori缩略图
//			$image = new \Think\Image();
//			$image->open($_POST['goods_img']);
			$image->thumb(100,100)->save($_POST['ori_img']); 
			
			$goodsModel = D('goods');
			//链接数据库
			if (!$goodsModel -> create()) {
				echo $goodsModel -> getError();
			} else {
				$goodsModel -> add();
				$this->success('新增商品成功','',2);
			}
		}
	}

	//商品列表
	public function goodslist() {
		//链接数据库
		$goodsModel = D('goods');
		//分页类
		$count = $goodsModel->count();
		$page = new \Think\Page($count,5);
		$show = $page->show();
		
		
		$data = $goodsModel -> field('goods_id,goods_name,goods_sn,shop_price,goods_number,is_on_sale,is_best,is_hot,is_new')->order('goods_id desc')->limit($page->firstRow.','.$page->listRows)->select();
		$this -> assign('page',$show);  
		$this -> assign('goods', $data);
		$this -> display();
	}

	//商品删除
	public function goodsdel() {
		$goodsModel = D('goods');
		$rest = $goodsModel -> delete(I('id'));
		if ($rest) {
			$this -> success('删除成功', '/index.php/Admin/goods/goodslist', 1);
		} else {
			echo $goodsModel -> getDbError();
		}
	}
	
	public function yzm(){
		$Verify = new \Think\Verify();
		$Verify->entry();
	}

}
