<?php
namespace admin\Controller;
use Think\Controller;
class catController extends Controller {
	//新增栏目
    public function catadd(){
    	if(IS_POST){
    		$catmodel = D('cat');
			$catmodel->add($_POST);
    	}
		$this->display();
    } 
	//展示栏目
	public function catlist(){
		$catmodel = D('cat');
		$catlist = $catmodel->getTree();
		$this->assign('catlist',$catlist);
		$this->display();  
	}
	//修改栏目  
	public function catdit(){
		$catmodel = D('cat'); //链接数据库
		$cat_id = I('cat_id');  //获取被修改的栏目ID
		
		if(!IS_POST){
			$info = $catmodel->find($cat_id); 
			$this->assign('info',$info);
			$this->display();
		}else{ 
			$rest = $catmodel->where('cat_id='.$cat_id)->save($_POST);
			if($rest){                      //如果新增成功
				$this->success('修改成功','',1);
			exit();
			}
			$this->success('修改失败','',1);
		}

	}
	//删除栏目
	public function catdel(){
		$catmodel = D('cat'); //链接数据库
		$cat_id = I('cat_id');  //获取被修改的栏目ID
		$rest=$catmodel->where('cat_id='.$cat_id)->delete();
		if($rest){                      //如果新增成功
				$this->success('删除成功','',1);
			exit();
			}
			echo "修改失败";
	}
}